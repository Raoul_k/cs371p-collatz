// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <climits> //for INT_MIN
#include <array>

#include "Collatz.h"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------
const int N = 1000000;
int collatz_arr[N + 1];

long helper_collatz_eval (long i, long j) {
    long max = INT_MIN;
    assert(i > 0 && j >0);
    assert(i < N);
    long x = i;
    while(x <= j) {
        long y = x;
        long count = 0;
        while(y >= 1) {

            if(y < N && collatz_arr[(int)y] > 0) {
                count += collatz_arr[(int)y];
                break;
            }

            if(y%2 != 0) {
                y = (3 * y + 1) / 2;
                ++++count;
            }
            else {
                y = y / 2;
                count++;
            }
        }


        if(x < N && collatz_arr[(int)x] < 0) {
            collatz_arr[(int)x] = count;
        }
        if(count > max) {
            max = count;
        }
        x++;
    }
    assert(max >= 1);
    return max;
}
int collatz_eval (int i, int j) {
    // <your code>
    assert(i > 0 && j >0);
    for(int w = 0; w < (N + 1); w++) {
        collatz_arr[w] = -1;
    }
    collatz_arr[1] = 1;
    if(i < j)
        return helper_collatz_eval(i, j);
    else
        return helper_collatz_eval(j, i);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
